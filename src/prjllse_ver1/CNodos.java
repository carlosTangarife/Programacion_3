/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prjllse_ver1;

/**
 *
 * @author Carlos Tangarife
 */
public class CNodos {
    private int info;
    private CNodos enlaceSig;

    /*Constructor Vacio*/
    public CNodos() {
        this.info = 0;
        this.enlaceSig = null;
    }

    /*Constructor con Parametros*/
    public CNodos(int info, CNodos enlaceSig) {
        this.info = info;
        this.enlaceSig = enlaceSig;
    }
    
    public int getInfo() {
        return info;
    }

    public void setInfo(int info) {
        this.info = info;
    }

    public CNodos getEnlaceSig() {
        return enlaceSig;
    }

    public void setEnlaceSig(CNodos enlaceSig) {
        this.enlaceSig = enlaceSig;
    }
    
}
