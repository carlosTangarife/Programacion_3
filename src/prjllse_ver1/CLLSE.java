/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prjllse_ver1;

import javax.swing.JOptionPane;

/**
 *
 * @author Carlos Tangarife
 */
public class CLLSE {

    private CNodos cab;
    private CNodos cab2;
    private CNodos cab3;

    public CNodos getCab3() {
        return cab3;
    }

    public void setCab3(CNodos cab3) {
        this.cab3 = cab3;
    }

    public CNodos getCab2() {
        return cab2;
    }

    public void setCab2(CNodos cab2) {
        this.cab2 = cab2;
    }

    public CNodos getCab() {
        return cab;
    }

    public void setCab(CNodos cab) {
        this.cab = cab;
    }

    public CLLSE(CNodos cab) {
        this.cab = cab;
    }

    public CLLSE() {
        this.cab = null;
    }

    String CreateList(int info) {
        String message = "No Exit List";
        if (cab == null) {
            CNodos objNewNode = new CNodos();
            objNewNode.setInfo(info);
            cab = objNewNode;
            message = "List Created Succesfully";
        } else {
            message = "There is Already a Head";
        }

        return message;
    }

    void CreateList2_cabeza(int info) {

        if (cab2 == null) {
            CNodos lista2 = new CNodos();
            lista2.setInfo(info);
            cab2 = lista2;
        }
    }

    String addNodeLeft(int nInfo) {
        String message = "por probar";
        if (cab != null) {
            CNodos objNewNode = new CNodos();
            objNewNode.setInfo(nInfo);
            objNewNode.setEnlaceSig(cab);
            cab = objNewNode;
            message = "Add node for Left Succefully";
        }
        return message;
    }

    String addNodeRigth(int nInfo) {
        String message = "Not is Posiblo Add Node To Rigth";
        if (cab != null) {
            CNodos tempFind = new CNodos();
            tempFind = cab;

            CNodos objNewNode = new CNodos();
            objNewNode.setInfo(nInfo);

            while (tempFind.getEnlaceSig() != null) {
                tempFind = tempFind.getEnlaceSig();
            }
            tempFind.setEnlaceSig(objNewNode);
            message = "Add node for Rigth Succefully";
        }
        return message;
    }

    void crea_contador(int info) {

        if (cab3 == null) {
            CNodos lista3 = new CNodos();
            lista3.setInfo(info);
            cab3 = lista3;
        }
    }

    /*Procesar Lista*/
    String procesar() {

        if (cab != null) {
            int total = 0;
            int info = 0;            
            CNodos tempFind = new CNodos();
            tempFind = cab;
            while (tempFind != null) {
                info = tempFind.getInfo();
                if (cab2 == null) {
                    CreateList2_cabeza(info);
                    crea_contador(procesar2(info));
                } else {
                    if(!validar(info)){
                        addNodeRigth_lista2(info);
                        addNodeRigth_contador(procesar2(info));
                    }
                }
                tempFind = tempFind.getEnlaceSig();
            }
        }
        return "holas";
    }

    int procesar2(int info) {
        int cantidad = 0;
        if (cab != null) {
            CNodos tempFind = new CNodos();
            tempFind = cab;
            while (tempFind != null) {
                if (info == tempFind.getInfo()) {
                    cantidad++;
                }
                tempFind = tempFind.getEnlaceSig();
            }
        }
        return cantidad;
    }

    boolean validar(int info) {
        boolean bandera = false;
        if (cab2 != null) {
            CNodos tempFind2 = new CNodos();
            tempFind2 = cab2;

            while (tempFind2 != null) {
                if (info == tempFind2.getInfo()) {
                    bandera = true;
                    break;
                }
                tempFind2 = tempFind2.getEnlaceSig();
            }
        }
        return bandera;
    }

    void addNodeRigth_lista2(int nInfo) {

        if (cab2 != null) {
            CNodos tempFind = new CNodos();
            tempFind = cab2;

            CNodos objNewNode = new CNodos();
            objNewNode.setInfo(nInfo);

            while (tempFind.getEnlaceSig() != null) {
                tempFind = tempFind.getEnlaceSig();
            }
            tempFind.setEnlaceSig(objNewNode);

        }
    }

    void addNodeRigth_contador(int nInfo) {
        if (cab3 != null) {
            CNodos tempFind = new CNodos();
            tempFind = cab3;

            CNodos objNewNode = new CNodos();
            objNewNode.setInfo(nInfo);

            while (tempFind.getEnlaceSig() != null) {
                tempFind = tempFind.getEnlaceSig();
            }
            tempFind.setEnlaceSig(objNewNode);
        }
    }

    //vamos a recorrer la lista
    String findList() {
        String mensaje = "";
        if (cab != null) {
            CNodos tempFind = new CNodos();
            tempFind = cab;
            while (tempFind != null) {
                mensaje += " - " + tempFind.getInfo();
                tempFind = tempFind.getEnlaceSig();
            }
        } else {
            mensaje = "Could Not Print Because There is no List";
        }
        return mensaje;
    }

    //vamos a recorrer la lista
    String findList2() {
        String mensaje = "";
        if (cab2 != null) {
            CNodos tempFind = new CNodos();
            tempFind = cab2;
            while (tempFind != null) {
                mensaje += " - " + tempFind.getInfo();
                tempFind = tempFind.getEnlaceSig();
            }
        } else {
            mensaje = "Could Not Print Because There is no List";
        }
        return mensaje;
    }

    //vamos a recorrer la lista
    String findList3() {
        String mensaje = "";
        if (cab3 != null) {
            CNodos tempFind = new CNodos();
            tempFind = cab3;
            while (tempFind != null) {
                mensaje += " - " + tempFind.getInfo();
                tempFind = tempFind.getEnlaceSig();
            }
        } else {
            mensaje = "Could Not Print Because There is no List";
        }
        return mensaje;
    }

    int countNodes() {
        int count = 0;
        if (cab != null) {
            CNodos tempFind = new CNodos();
            tempFind = cab;

            while (tempFind != null) {
                tempFind = tempFind.getEnlaceSig();
                count += 1;
            }
        }
        return count;
    }

    int sumList() {
        int message = 0;
        if (cab != null) {
            CNodos tempFind = new CNodos();
            tempFind = cab;
            message = tempFind.getInfo();
            while (tempFind.getEnlaceSig() != null) {
                tempFind = tempFind.getEnlaceSig();
                message += tempFind.getInfo();
            }
        }
        return message;
    }

    double avg() {
        return sumList() / countNodes();
    }

    int findNode(int num) {
        int pos = 0;
        int cont = 0;
        if (cab != null) {
            CNodos tempFind = new CNodos();
            tempFind = cab;

            while (tempFind != null) {
                cont++;
                if (num == tempFind.getInfo()) {
                    pos = cont;
                    break;
                }
                tempFind = tempFind.getEnlaceSig();
            }
        }
        return pos;
    }

    int dropNodeTest(int num) {
        cab = null;

        for (int i = 1; i < 7; i++) {
            CNodos objNewNode1 = new CNodos();
            objNewNode1.setInfo(i);
            objNewNode1.setEnlaceSig(cab);
            cab = objNewNode1;
        }

        return 0;
    }

    int dropNode(int num) {
        int cont = 0;
        int answer = 0;
        if (cab != null) {
            CNodos tempFind = new CNodos();
            tempFind = cab;

            while (tempFind != null) {
                cont++;
                CNodos objNewNode = new CNodos();
                objNewNode.setInfo(tempFind.getInfo());
                objNewNode.setEnlaceSig(cab);
                cab = objNewNode;
                if (cont == num) {
                    objNewNode.setEnlaceSig(cab);
                    JOptionPane.showMessageDialog(null, "nodo existe# = >" + cont + "Dato =>" + tempFind.getInfo());
                } else {
                    //JOptionPane.showMessageDialog(null, "nodo # = >"+ cont + "Dato =>"+tempFind.getInfo());
                }
                tempFind = tempFind.getEnlaceSig();
            }

        }
        return answer;
    }

    void invertir() {
        if (cab != null) {
            CNodos tempFind = new CNodos();
            tempFind = cab;
            cab = null;

            while (tempFind != null) {
                CNodos obj = new CNodos();
                obj.setInfo(tempFind.getInfo());
                obj.setEnlaceSig(cab);
                cab = obj;
                tempFind = tempFind.getEnlaceSig();
            }
        }
    }

    void deleteAllNode() {
        cab = null;
    }

}
